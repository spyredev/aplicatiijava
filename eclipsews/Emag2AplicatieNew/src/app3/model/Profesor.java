package app3.model;

import java.util.List;

public class Profesor {

	private Integer id;
	private String numeProf;
	
	private List<Curs> cursuri;
	
	public Profesor(){
		
	}
	
	public Profesor(Integer id,String numeProf){
		this.id=id;
		this.numeProf=numeProf;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumeProf() {
		return numeProf;
	}

	public void setNumeProf(String numeProf) {
		this.numeProf = numeProf;
	}

	public List<Curs> getCursuri() {
		return cursuri;
	}

	public void setCursuri(List<Curs> cursuri) {
		this.cursuri = cursuri;
	}
	
	
}
