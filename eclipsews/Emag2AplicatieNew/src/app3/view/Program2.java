package app3.view;

import java.sql.SQLException;
import java.util.List;

import app3.db.DBCursuri;
import app3.db.DBProfesori;
import app3.model.Curs;
import app3.model.Profesor;

public class Program2 {

	public static void main(String[] args) throws SQLException {
		
		List<Profesor> profi = null;
		List<Curs> cursuri = DBCursuri.findAll();
		System.out.println(cursuri.get(0).getId());
		profi = DBProfesori.findByCurs(cursuri.get(0));
		for(Profesor prof : profi){
			System.out.println(prof.getNumeProf() + " preda " + cursuri.get(0).getNumeCurs());
		}
		
		System.out.println("------");
		System.out.println(profi.get(0).getCursuri());
		
		System.out.println("----------------------------profi si cursuri-------------------------");
		for(Curs c : cursuri){
			System.out.println("Cursul este: " + c.getNumeCurs() + " este predat de: ");
			List<Profesor> profiCurs = DBProfesori.findByCurs(c);
			for(Profesor prof : profiCurs){
				System.out.println("\t\t" + prof.getNumeProf());
			}
		}
	}
	
}
