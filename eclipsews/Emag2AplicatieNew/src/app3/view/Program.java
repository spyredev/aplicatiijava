package app3.view;

import java.sql.SQLException;
import java.util.List;

import app3.db.DBCursuri;
import app3.db.DBProfesori;
import app3.model.Curs;
import app3.model.Profesor;

public class Program {

	public static void main(String[] args) throws SQLException{
		List<Profesor> profesori=DBProfesori.findAll();
		for (Profesor profesor : profesori){
			System.out.println(profesor.getNumeProf());
		}
		Profesor profesor=DBProfesori.findbyId(1);
		System.out.println(profesor.getNumeProf());
		List<Curs> cursuri=DBCursuri.findAll();
		for (Curs curs : cursuri){
			System.out.println(curs.getNumeCurs());
		}
		Curs curs=DBCursuri.findbyId(2);
		System.out.println(curs.getNumeCurs());
		
	}
}
