package app3.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.IConstante;
import app3.model.Curs;

public class DBCursuri {

	public static List<Curs> findAll() throws SQLException {
		List<Curs> cursuri = new ArrayList<>();
		Curs curs;
		Connection connection = MySQLConnectionProvider.getInstance().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from cursuri");
		while (result.next()) {
			curs = new Curs();
			curs.setId(result.getInt("id"));
			curs.setNumeCurs(result.getString("numecurs"));
			cursuri.add(curs);
		}
		return cursuri;
	}

	public static Curs findbyId(Integer id) throws SQLException {
		Curs curs;
		Connection connection = MySQLConnectionProvider.getInstance().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from cursuri where id = " + id);
		while (result.next()) {
			curs = new Curs();
			curs.setId(result.getInt("id"));
			curs.setNumeCurs(result.getString("numecurs"));
			return curs;
		}
		return null;
	}

}
