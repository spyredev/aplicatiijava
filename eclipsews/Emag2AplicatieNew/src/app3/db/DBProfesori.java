package app3.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import app.IConstante;
import app3.model.Curs;
import app3.model.Profesor;

public class DBProfesori {

	public static List<Profesor> findAll() throws SQLException {
		List<Profesor> profesori = new ArrayList<>();
		Profesor profesor;
		Connection connection = MySQLConnectionProvider.getInstance().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from profesori");
		while (result.next()) {
			profesor = new Profesor();
			profesor.setId(result.getInt("id"));
			profesor.setNumeProf(result.getString("numeprof"));
			profesori.add(profesor);
		}
		return profesori;
	}

	public static Profesor findbyId(Integer id) throws SQLException {
		Profesor profesor;
		Connection connection = MySQLConnectionProvider.getInstance().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery("select * from profesori where id = " + id);
		while (result.next()) {
			profesor = new Profesor();
			profesor.setId(result.getInt("id"));
			profesor.setNumeProf(result.getString("numeprof"));
			return profesor;
		}
		return null;
	}

	public static List<Profesor> findByCurs(Curs curs) throws SQLException { // equals,
																				// hashCode
		int idCurs = curs.getId();
		String query = "select profesori.* from profesori inner join profi_cursuri on profi_cursuri.idprof = profesori.id where profi_cursuri.idcurs = "
				+ curs.getId();
		List<Profesor> profesori = new ArrayList<>();

		Connection connection = MySQLConnectionProvider.getInstance().getConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery(query);
		while (result.next()) {
			Profesor profesor = new Profesor();
			profesor.setId(result.getInt("id"));
			profesor.setNumeProf(result.getString("numeprof"));
			profesori.add(profesor);
		}
		return profesori;
	}

}
