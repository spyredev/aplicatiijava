package app3.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import app.IConstante;

public class MySQLConnectionProvider {

	private static MySQLConnectionProvider theInstance = null;
	private Connection connection = null;

	private MySQLConnectionProvider() throws SQLException {
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
	}

	public static MySQLConnectionProvider getInstance() {
		if (theInstance == null) {
			try {
				theInstance = new MySQLConnectionProvider();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return theInstance;
	}
	
	public Connection getConnection(){
		return connection;
	}

}
