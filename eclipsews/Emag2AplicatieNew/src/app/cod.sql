SELECT * FROM emag2.clienti;

alter table clienti add column email varchar(40);
alter table clienti add column email2 varchar(40);

alter table clienti drop column email;

update clienti set email = 'ion@gmail.com' where id = 4;

update clienti set email = 'jim@jimmail.com' where id = 7;

create table emailuri(id int primary key auto_increment, email varchar(50) not null, id_owner int);

alter table emailuri add constraint fk_ceva foreign key(id_owner) references clienti(id);


create table emailuri(id int primary key auto_increment, email varchar(50) not null, id_owner int, foreign key(id_owner) references clienti(id));

select * from clienti;
select * from emailuri;

insert into emailuri(email) values('spire@yahoo.com');

select clienti.numeClient, clienti.username, emailuri.email from clienti 
			inner join emailuri on emailuri.id_owner = clienti.id where clienti.numeClient = 'ion';
            
            

insert into emailuri(email, id_owner) values('ion@gmail.com', 4), ('ion@yahoo.com', 4), ('geo@geomail.us', 6);

insert into emailuri(email, id_owner) values('razvan@geocities.us', 787);
update emailuri set id_owner = 6 where id = 4;

-- toti clientii, chiar daca nu au email-uri
select clienti.numeClient, clienti.username, emailuri.email from clienti left join emailuri on emailuri.id_owner = clienti.id;

-- toate email-urile, chiar daca nu au clienti
select clienti.numeClient, clienti.username, emailuri.email from clienti right join emailuri on emailuri.id_owner = clienti.id;

-- nu se poate :(
select clienti.numeClient, clienti.username, emailuri.email from clienti full join emailuri on emailuri.id_owner = clienti.id;

-- "hack" pentru full join
select clienti.numeClient, clienti.username, emailuri.email from clienti left join emailuri on emailuri.id_owner = clienti.id
union
select clienti.numeClient, clienti.username, emailuri.email from clienti right join emailuri on emailuri.id_owner = clienti.id;

select * from clienti;

select * from emailuri;

-- vanzari
-- 		un produs (sau mai multe) pot fi vandute unui client
-- 		ce produs a fost vandut carui client?
--		many to many (un produs poate fi vandut mai multor clienti si un client poate cumpara mai multe produse)



	Clienti
ID	NUME
1	ion
2	vasile
3	geo
4	jim


	Emailuri		
ID	EMAIL 				ID_OWNER
1	ion@gmail.com			1
2	ion@pizzahut.ro			1
3	ion@yahoo.com			1
4	geo@geomail.us			3
5	spyre@spyremail.jp		87
6
7

mysql> create database emag2;
Query OK, 1 row affected (0.00 sec)

mysql> use emag2;
Database changed

mysql> create table clienti(id_client int primary key auto_increment, nume_clien
t varchar(255), check(id_client>0));
Query OK, 0 rows affected (0.10 sec)

mysql> insert into clienti(nume_client) values ('andrei'),('andreea'),('maria');

Query OK, 3 rows affected (0.01 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> select * from clienti;
+-----------+-------------+
| id_client | nume_client |
+-----------+-------------+
|         1 | andrei      |
|         2 | andreea     |
|         3 | maria       |
+-----------+-------------+
3 rows in set (0.00 sec)

mysql> create table produse(id_produs int auto_increment primary key, nume_produ
s varchar(255), pret double);
Query OK, 0 rows affected (0.07 sec)

mysql> insert into produse(nume_produs,pret) values('calculator',2500.0),('lapto
p',3400.0),('router',650.0);
Query OK, 3 rows affected (0.01 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> select * from produse;
+-----------+-------------+------+
| id_produs | nume_produs | pret |
+-----------+-------------+------+
|         1 | calculator  | 2500 |
|         2 | laptop      | 3400 |
|         3 | router      |  650 |
+-----------+-------------+------+
3 rows in set (0.00 sec)

mysql> create table vanzari(client_id int, constraint a foreign key (client_id)
references clienti(id_client), produs_id int, constraint b foreign key (produs_i
d) references produse(id_produs));
Query OK, 0 rows affected (0.09 sec)

mysql> insert into vanzari values (1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,3);
Query OK, 7 rows affected (0.03 sec)
Records: 7  Duplicates: 0  Warnings: 0

mysql> select * from vanzari;
+-----------+-----------+
| client_id | produs_id |
+-----------+-----------+
|         1 |         2 |
|         1 |         3 |
|         2 |         1 |
|         2 |         2 |
|         2 |         3 |
|         3 |         1 |
|         3 |         3 |
+-----------+-----------+
7 rows in set (0.00 sec)

mysql> select clienti.nume_client,produse.nume_produs,produse.pret from clienti
left join vanzari on clienti.id_client=vanzari.client_id left join produse on pr
oduse.id_produs=vanzari.produs_id;
+-------------+-------------+------+
| nume_client | nume_produs | pret |
+-------------+-------------+------+
| andreea     | calculator  | 2500 |
| maria       | calculator  | 2500 |
| andrei      | laptop      | 3400 |
| andreea     | laptop      | 3400 |
| andrei      | router      |  650 |
| andreea     | router      |  650 |
| maria       | router      |  650 |
+-------------+-------------+------+
7 rows in set (0.00 sec)

mysql> select clienti.nume_client,produse.nume_produs,produse.pret from clienti
left join vanzari on clienti.id_client=vanzari.client_id left join produse on pr
oduse.id_produs=vanzari.produs_id order by clienti.nume_client;
+-------------+-------------+------+
| nume_client | nume_produs | pret |
+-------------+-------------+------+
| andreea     | calculator  | 2500 |
| andreea     | laptop      | 3400 |
| andreea     | router      |  650 |
| andrei      | laptop      | 3400 |
| andrei      | router      |  650 |
| maria       | calculator  | 2500 |
| maria       | router      |  650 |
+-------------+-------------+------+
7 rows in set (0.00 sec)
























insert into profesori(nume) values('ion'), ('vasile'), ('jim');

create table profesori(id int primary key auto_increment, nume varchar(240) not null);
create table cursuri(id int primary key auto_increment, denumireCurs varchar(240) not null, id_prof int,
			foreign key(id_prof) references profesori(id));
            
select * from profesori;
select * from cursuri;

insert into cursuri(denumireCurs, id_prof) values('java', 1), ('php', 1), ('sql', 2);
-- drop table profesori;
-- drop table cursuri;

insert into profesori(id, nume) values (-30, 'billy');

select profesori.nume, cursuri.denumireCurs from profesori inner join cursuri on profesori.id = cursuri.id_prof;

select * from profesori;


-- TODO: please do the same
create table autori(id int primary key auto_increment, nume_autor varchar(240) not null);
create table carti(id int primary key auto_increment, titlu varchar(240) not null, id_autor int, -- FK 







mysql> insert into autori (nume_autor) values ('daniel keyes'),('lois lowry');
Query OK, 2 rows affected (0.02 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select * from autori;
+----+--------------+
| id | nume_autor   |
+----+--------------+
|  1 | daniel keyes |
|  2 | lois lowry   |
+----+--------------+
2 rows in set (0.00 sec)

mysql> select * from autori;
+----+--------------+
| id | nume_autor   |
+----+--------------+
|  1 | daniel keyes |
|  2 | jim          |
|  3 | andrei       |
+----+--------------+
3 rows in set (0.00 sec)

mysql> select * from autori;
+----+--------------+
| id | nume_autor   |
+----+--------------+
|  1 | daniel keyes |
|  2 | jim          |
|  3 | adrian       |
+----+--------------+
3 rows in set (0.00 sec)

mysql> create table carti(id int primary key auto_increment,titlu varchar(240) n
ot null,id_autor int, foreign key(id_autor) references autori(id));
Query OK, 0 rows affected (0.09 sec)

mysql> insert into carti(titlu,id_autor) values('a',1),('b',2),('c',3),('d',4),(
'e',5);
Query OK, 5 rows affected (0.02 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from carti;
+----+-------+----------+
| id | titlu | id_autor |
+----+-------+----------+
|  1 | a     |        1 |
|  2 | b     |        2 |
|  3 | c     |        3 |
|  4 | d     |        4 |
|  5 | e     |        5 |
+----+-------+----------+
5 rows in set (0.00 sec)

