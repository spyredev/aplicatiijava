package app;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class Program2 {

	public static void main(String[] args) throws SQLException {
		
		
		/*List<Produs> produse = DBProduse.findAll();
		for(Produs prod : produse){
			System.out.println(prod.getNume() + " " + prod.getPret());
		}
		
		System.out.println("-------");
		for(Produs prod : produse){
			System.out.println("ID: " + prod.getId() + " nume : " + prod.getNume() + " pret: " + prod.getPret());			
		}
		System.out.println("-------");
		
		List<Produs> produseScumpe = produse.stream().filter((p) -> p.getPret() > 200).collect(Collectors.toList());
		
		for(Produs prod : produseScumpe){
			System.out.println("ID: " + prod.getId() + " nume : " + prod.getNume() + " pret: " + prod.getPret());			
		}
		
		System.out.println("CLIENTI: ------------------------");
		List<Client> clienti=DBClienti.findAll();
		clienti.stream().map(Client::getNumeClient).forEach(System.out::println);
		System.out.println("Login: ------------------------");
		
		String usernameOK = "ion1";
		String passwordOK = "1234";
		
		List<Client> clientiAutorizati = clienti.stream().filter((a)-> a.getUsername() != null && a.getPassword() != null && a.getUsername().equals(usernameOK) && a.getPassword().equals(passwordOK)).collect(Collectors.toList());
		System.out.println(clientiAutorizati.get(0).getNumeClient());
		
		System.out.println("---");
//		clienti.stream().filter((a)->a.getUsername().equals(usernameOK) && a.getPassword().equals(passwordOK)).map(Client::getNumeClient).forEach(System.out::println);//collect(Collectors.toList());
		
		if(DBClienti.login("ion1", "1234x")){
			System.out.println("Ai acces");
		}else{
			System.out.println("Nu ai acces");
		}*/
		System.out.println("---");
		System.out.println(DBProduse.findById(6));
		System.out.println(DBProduse.findByName("b"));
		System.out.println(DBProduse.findAllProduseInInterval(100, 300));
		System.out.println(DBClienti.findById(1));
	}
	
}
