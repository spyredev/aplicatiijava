package app;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Program3 {

	public static void main(String[] args) throws SQLException {
//		
//		Profesor prof = new Profesor();
//		prof.setNume("Ionescu");
//		prof.setId(1032);
//		
		// cand ai un profesor vrei sa-l adaugi sau sa-l update-ezi in BD
//		if(prof.getId() != null){
//			// proful are deja un id, deci exista in baza de date, deci dam update
//			String query = "UPDATE profesori set nume = '" + prof.getNume() + "' where id = " + prof.getId();
//			System.out.println("Executam: " + query);
//		}else{
//			String query = "INSERT INTO profesori(nume) values('" + prof.getNume() + "')";
//			System.out.println("EXECUTAM: " + query);
//		}
		
		System.out.println("----------------");
		List<Profesor> profi = DBProfesori.findAll();
		profi.stream().map(Profesor::getNume).forEach(System.out::println);
		
		System.out.println("Doresti sa 1. adaugi un profesor 2. modifici un profesor");
		Scanner scan = new Scanner(System.in);
		int raspuns = scan.nextInt();
		if(raspuns == 1){
			System.out.println("Numele profului nou: ");
			String nume = scan.next();
			Profesor profNou = new Profesor();
			profNou.setNume(nume);
			DBProfesori.saveOrUpdate(profNou);
		}else if(raspuns == 2){
			// update
			System.out.println("ID-ul profului existent: ");
			int id = scan.nextInt();
			System.out.println("Numele nou este: ");
			String numeNou = scan.next();
			Profesor profExistent = DBProfesori.findAll().stream().filter( (p) -> {return p.getId() == id; }).collect(Collectors.toList()).get(0);
			profExistent.setNume(numeNou);
			DBProfesori.saveOrUpdate(profExistent);
		}
	}
	
}
