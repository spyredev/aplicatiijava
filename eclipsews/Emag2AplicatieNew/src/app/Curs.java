package app;

public class Curs {

	private Integer id;
	private String denumireCurs;
	private Integer idProf;  // lol
	
	/* MANY TO ONE */
	private Profesor profesor; // profesorul care tine cursul in loc de un
								// Integer

	public Curs(Integer id, String denumireCurs) {
		this.id = id;
		this.denumireCurs = denumireCurs;
	}

	public Curs() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDenumireCurs() {
		return denumireCurs;
	}

	public void setDenumireCurs(String denumireCurs) {
		this.denumireCurs = denumireCurs;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public Integer getIdProf() {
		return idProf;
	}

	public void setIdProf(Integer idProf) {
		this.idProf = idProf;
	}

}
