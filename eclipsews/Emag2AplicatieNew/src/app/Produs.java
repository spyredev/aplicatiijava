package app;

public class Produs {

	private Integer id;
	private String nume;
	private double pret;
	
	public Produs(){
		
	}
	
	
	
	public Produs(Integer id, String nume, double pret) {
		this.id = id;
		this.nume = nume;
		this.pret = pret;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public double getPret() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret = pret;
	}



	@Override
	public String toString() {
		return "Produs [id=" + id + ", nume=" + nume + ", pret=" + pret + "]";
	}
	
	
	
	
}
