package app;

import java.util.List;

public class Profesor {

	private Integer id;
	private String nume;
	private List<Curs> cursuri;
	
	public Profesor(Integer id, String nume) {
		this.id = id;
		this.nume = nume;
	}

	public Profesor(){
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}

	public List<Curs> getCursuri() {
		return cursuri;
	}

	public void setCursuri(List<Curs> cursuri) {
		this.cursuri = cursuri;
	}
	
	
	
}
