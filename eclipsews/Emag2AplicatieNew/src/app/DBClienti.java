package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBClienti {

	public static boolean login(String username, String password) throws SQLException {
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery(
				"select * from clienti where username = '" + username + "' and password = '" + password + "'"); // SELECT
		while (rezultate.next()) {
			return true;
		}
		return false;
	}

	public static List<Client> findAll() throws SQLException {
		List<Client> clienti = new ArrayList<>();

		/// ...
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from clienti"); // SELECT
		while (rezultate.next()) {

			Client client = new Client();
			client.setId(rezultate.getInt("id"));
			client.setNumeClient(rezultate.getString("numeClient"));
			client.setUsername(rezultate.getString("username"));
			client.setPassword(rezultate.getString("password"));
			
			client.setEmails(getEmailsForClient(client.getId()));
			
			clienti.add(client);
		}
		return clienti;
	}
	
	public static List<String> getEmailsForClient(int clientId) throws SQLException{
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		
		List<String> emails = new ArrayList<>();
		ResultSet rezultate = conexiune.createStatement().executeQuery("SELECT * FROM Emailuri WHERE id_owner = " + clientId);
		while(rezultate.next()){
			emails.add(rezultate.getString("email"));
		}
		
		return emails;
	}

	public static Client findById(int id) throws SQLException { // tema
		// direct elementul de interes (cu where)
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from clienti where id = " + id);
		while (rezultate.next()) {
			Client client = new Client();
			client.setId(rezultate.getInt("id"));
			client.setNumeClient(rezultate.getString("numeClient"));
			client.setUsername(rezultate.getString("username"));
			client.setPassword(rezultate.getString("password"));
			return client;
		}
		return null;
	}

	public static void main(String[] args) throws SQLException {
		List<Client> clienti = findAll();
		for(Client client : clienti){
			System.out.println(client);
		}
		
		System.out.println(getEmailsForClient(1));
	}
}
