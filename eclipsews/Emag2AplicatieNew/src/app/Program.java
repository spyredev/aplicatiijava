package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class Program {

	public static void main(String[] args) throws Exception {

		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/emag", "root", "andrei");

		Statement stmt = conexiune.createStatement();

		String comanda2 = "INSERT INTO PRODUSE(nume_produs, pret) values('guma sters 3', 1.0)";

		boolean succes = true;
		try {
			stmt.executeUpdate(comanda2); // INSERT UPDATE DELETE
		} catch (MySQLIntegrityConstraintViolationException e) {
			System.out.println("Nu am putut insera randul.");
			succes = false;
		}
		
		if(succes){
			System.out.println("Am inserat cu succes");
		}

		ResultSet rezultate = stmt.executeQuery("select * from produse"); // SELECT
		while (rezultate.next()) {
			System.out.println("ID: " + rezultate.getInt("id") + " nume: " + rezultate.getString("nume_produs")
					+ " pret: " + rezultate.getDouble("pret"));
		}

		System.out.println("END PROGRAM");

	}
}
