package app;

import java.util.ArrayList;
import java.util.List;

public class Client {

	private Integer id;
	private String numeClient;
	private String username; // optionale
	private String password; // optionale
	
	private List<String> emails = new ArrayList<>(); // populate de metoda getEmailsBy...
	
	
	public Client(Integer id, String numeClient, String username, String password) {
		this.id = id;
		this.numeClient = numeClient;
		this.username = username;
		this.password = password;
	}
	
	
	
	public Client(Integer id, String numeClient) {
		this.id = id;
		this.numeClient = numeClient;
	}

	
	public Client(){
		
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNumeClient() {
		return numeClient;
	}
	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}



	public List<String> getEmails() {
		return emails;
	}



	public void setEmails(List<String> emails) {
		this.emails = emails;
	}



	@Override
	public String toString() {
		return "Client [id=" + id + ", numeClient=" + numeClient + ", username=" + username + ", password=" + password
				+ ", emails=" + emails + ", getId()=" + getId() + ", getNumeClient()=" + getNumeClient()
				+ ", getUsername()=" + getUsername() + ", getPassword()=" + getPassword() + ", getEmails()="
				+ getEmails() + "]";
	}
	
	
	
	
}
