package app;

import java.sql.SQLException;
import java.util.List;

public class Program5 {

	public static void main(String[] args) throws SQLException {
		
		List<Profesor> profi = DBProfesori.findAll();
		for(Profesor prof : profi){
			System.out.println("ID: " + prof.getId() + ", NUME: " + prof.getNume() + " --- preda: " + prof.getCursuri());
		}
		
	}
	
}
