package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBProduse {

	public static List<Produs> findAll() throws SQLException {
		
		List<Produs> produseDinDB = new ArrayList<>();
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root", IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from produse"); // SELECT
		while (rezultate.next()) {
//			System.out.println(" nume: " + rezultate.getString("nume_produs")
//					+ " pret: " + rezultate.getDouble("pret"));
			
			Produs produs = new Produs();
			produs.setId(rezultate.getInt("id"));
			produs.setNume(rezultate.getString("nume_produs"));
			produs.setPret(rezultate.getDouble("pret"));
			
			produseDinDB.add(produs);
		}
		
	
		return produseDinDB;
	}
	
	public static Produs findById(int id) throws SQLException{ //tema
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+ IConstante.NUMEDB, "root", IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from produse where id="+id);
		while(rezultate.next()){
			Produs produs = new Produs();
			produs.setId(rezultate.getInt("id"));
			produs.setNume(rezultate.getString("nume_produs"));
			produs.setPret(rezultate.getDouble("pret"));
			return produs;
		}
		return null;
	}
	
	public static Produs findByName(String numeProdus) throws SQLException{ //tema
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+ IConstante.NUMEDB, "root", IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from produse where nume_produs='"+numeProdus+"'");
		while(rezultate.next()){
			Produs produs = new Produs();
			produs.setId(rezultate.getInt("id"));
			produs.setNume(rezultate.getString("nume_produs"));
			produs.setPret(rezultate.getDouble("pret"));
			return produs;
		}
		return null;
	}
	
	public static List<Produs> findAllProduseInInterval(double pretStart, double pretEnd) throws SQLException{ //tema
		List<Produs> produse=new ArrayList<>();
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+ IConstante.NUMEDB, "root", IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from produse where pret between "+pretStart+" and "+pretEnd);
		while (rezultate.next()){
			Produs produs = new Produs();
			produs.setId(rezultate.getInt("id"));
			produs.setNume(rezultate.getString("nume_produs"));
			produs.setPret(rezultate.getDouble("pret"));
			produse.add(produs);
		}
		return produse;
	}
	
	public static void main(String[] args) throws SQLException {
		List<Produs> produseGasite = findAllProduseInInterval(10.00, 299.99);
		
		produseGasite.stream().map(Produs::getNume).forEach(System.out::println);
		
		System.out.println(findAllProduseInInterval(10.00, 299.99));
		
		System.out.println("-------");
		System.out.println(findByName("chitara").getPret());
		
	}

}
