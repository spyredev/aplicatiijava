package app;

import java.sql.SQLException;
import java.util.List;

public class Program4 {

	public static void main(String[] args) throws SQLException {
		
		List<Curs> cursuri = DBCurs.findAll();
		for(Curs curs : cursuri){
			System.out.println(curs.getId() + " " + curs.getDenumireCurs() + " " + curs.getProfesor() + " " + curs.getIdProf());
		}
		
		System.out.println("---------");
		Curs curs = DBCurs.findById(1);
		System.out.println(curs.getId() + " " + curs.getDenumireCurs() + " " + curs.getProfesor() + " " + curs.getIdProf());
		
		System.out.println("--------------");
		List<Profesor> profi = DBProfesori.findAll();
		Profesor profCautat = null;
		
		for(Profesor prof : profi){
			if(prof.getNume().equals("ion")){
				profCautat = prof;
				break;
			}
		}
		
		
		for(Curs c : cursuri){
			if(c.getIdProf().equals(profCautat.getId())){
				System.out.println("NUME CURS PREDAT DE ion: " + c.getDenumireCurs());
			}
		}
		
		System.out.println("--------------");   // datorita liniei de cod curs.setProfesor(DBProfesori.findById(rezultate.getInt("id_prof")));
		for(Curs c : cursuri){
			if(c.getProfesor().getNume().equals("ion")){
				System.out.println("NUME CURS PREDAT DE ion: " + c.getDenumireCurs());
			}
		}
	}
	
}
