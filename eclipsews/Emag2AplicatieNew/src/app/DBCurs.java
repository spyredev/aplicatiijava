package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBCurs {

	public static List<Curs> findAll() throws SQLException {

		List<Curs> cursuriDinDB = new ArrayList<>();
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from cursuri"); // SELECT
		while (rezultate.next()) {

			Curs curs = new Curs();
			curs.setId(rezultate.getInt("id"));
			curs.setDenumireCurs(rezultate.getString("denumireCurs"));
			curs.setIdProf(rezultate.getInt("id_prof")); // lol
			
			curs.setProfesor(DBProfesori.findById(rezultate.getInt("id_prof")));
			cursuriDinDB.add(curs); 
		}

		return cursuriDinDB;
	}

	public static Curs findById(int id) throws SQLException { // tema
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from cursuri where id=" + id);
		while (rezultate.next()) {
			Curs curs = new Curs();
			curs.setId(rezultate.getInt("id"));
			curs.setDenumireCurs(rezultate.getString("denumireCurs"));
			curs.setIdProf(rezultate.getInt("id_prof")); // lol
			return curs;
		}
		return null;
	}
	
	public static List<Curs> findByProfId(int id) throws SQLException { // tema
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		
		List<Curs> cursuriPredate = new ArrayList<>();
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from cursuri where id_prof = " + id);
		while (rezultate.next()) {
			Curs curs = new Curs();
			curs.setId(rezultate.getInt("id"));
			curs.setDenumireCurs(rezultate.getString("denumireCurs"));
			curs.setIdProf(rezultate.getInt("id_prof")); // lol
			cursuriPredate.add(curs);
		}
		return cursuriPredate;
	}
	
}
