package app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBProfesori {

	public static List<Profesor> findAll() throws SQLException {

		List<Profesor> profesoriDinDB = new ArrayList<>();
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from profesori"); // SELECT
		while (rezultate.next()) {
			// System.out.println(" nume: " + rezultate.getString("nume_produs")
			// + " pret: " + rezultate.getDouble("pret"));

			Profesor profesor = new Profesor();
			profesor.setId(rezultate.getInt("id"));
			profesor.setNume(rezultate.getString("nume"));

			profesor.setCursuri(DBCurs.findByProfId(profesor.getId()));
//			List<Curs> cursuriToate = DBCurs.findAll();
//			List<Curs> cursuriProfCurent = new ArrayList<>();
//			for(Curs c : cursuriToate){
//				if(c.getProfesor().getId().equals(profesor.getId())){
//					cursuriProfCurent.add(c);
//				}
//			}
//			profesor.setCursuri(cursuriProfCurent);
			
			profesoriDinDB.add(profesor);
		}

		return profesoriDinDB;
	}

	public static Profesor findById(int id) throws SQLException { // tema
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		System.out.println("Conexiune noua tocmai fu creata");
		Statement stmt = conexiune.createStatement();
		ResultSet rezultate = stmt.executeQuery("select * from profesori where id=" + id);
		while (rezultate.next()) {
			Profesor profesor = new Profesor();
			profesor.setId(rezultate.getInt("id"));
			profesor.setNume(rezultate.getString("nume"));
			
//			List<Curs> cursuriToate = DBCurs.findAll();
//			List<Curs> cursuriProfCurent = new ArrayList<>();
//			for(Curs c : cursuriToate){
//				if(c.getProfesor().getId().equals(profesor.getId())){
//					cursuriProfCurent.add(c);
//				}
//			}
//			profesor.setCursuri(cursuriProfCurent);
			profesor.setCursuri(DBCurs.findByProfId(profesor.getId()));
			
			return profesor;
		}
		return null;
	}

	public static void saveProf(Profesor prof) throws SQLException {
		String query = "INSERT INTO profesori(nume) values('" + prof.getNume() + "')";
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		Statement stmt = conexiune.createStatement();
		stmt.executeUpdate(query);
	}

	public static void updateProf(Profesor prof) throws SQLException {
		String query = "UPDATE profesori set nume = '" + prof.getNume() + "' where id = " + prof.getId();
		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		Statement stmt = conexiune.createStatement();
		stmt.executeUpdate(query);
	}

	public static void saveOrUpdate(Profesor prof) throws SQLException {

		String query = null;
		if (prof.getId() != null) {
			query = "UPDATE profesori set nume = '" + prof.getNume() + "' where id = " + prof.getId();
		} else {
			query = "INSERT INTO profesori(nume) values('" + prof.getNume() + "')";
		}

		Connection conexiune = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + IConstante.NUMEDB, "root",
				IConstante.PASSWORD);
		Statement stmt = conexiune.createStatement();
		stmt.executeUpdate(query);
	}
}
