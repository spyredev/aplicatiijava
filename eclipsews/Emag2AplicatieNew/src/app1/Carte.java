package app1;

public class Carte {
	private Integer id;
	private String titlu;
	
	private Autor autor;
	
	public Carte(){
		
	}
	
	public Carte(Integer id,String titlu){
		this.id=id;
		this.titlu=titlu;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitlu() {
		return titlu;
	}
	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}
	
	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	@Override
	public String toString() {
		return "id=" + id + ", titlu=" + titlu + ", autor=" + autor;
	}
	
}
