package app1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import app.IConstante;

public class DBAutori {
	
	// cand iei toti autorii iei si toate cartile
	public static List<Autor> findAll() throws SQLException{
		ResultSet result=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD).createStatement().executeQuery("select * from autori");
		System.out.println("Conexiunea noua tocmai fu creata");
		List<Autor> autori=new ArrayList<>();
		while(result.next()){
			Autor autor=new Autor();
			autor.setId(result.getInt("id"));
			autor.setNume(result.getString("nume_autor"));
			autor.setCarti(DBCarti.findByAutorId(autor.getId()));
			autori.add(autor);
		}
		return autori;
	}
	
	public static Autor findById(Integer id) throws SQLException{
		ResultSet result=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD).createStatement().executeQuery("select * from autori where id = "+id);
		System.out.println("Conexiunea noua tocmai fu creata");
		while(result.next()){
			Autor autor=new Autor();
			autor.setId(result.getInt("id"));
			autor.setNume(result.getString("nume_autor"));
			autor.setCarti(DBCarti.findByAutorId(autor.getId()));
			return autor;
		}
		return null;
	}
	
	public static Autor findByName(String name) throws SQLException{
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
				 IConstante.NUMEDB,"root", IConstante.PASSWORD);
		PreparedStatement stmt = conn.prepareStatement("select * from autori where nume_autor = ?");
		stmt.setString(1, name);
		
		ResultSet result = stmt.executeQuery();
		System.out.println("Conexiunea noua tocmai fu creata");
		while(result.next()){
			Autor autor=new Autor();
			autor.setId(result.getInt("id"));
			autor.setNume(result.getString("nume_autor"));
			autor.setCarti(DBCarti.findByAutorId(autor.getId()));
			return autor;
		}
		return null;
	}
	
	public static void saveorupdateAutori(Autor autor) throws SQLException{
		Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD);
		Statement statement=connection.createStatement();
		if(autor.getId()!=null){
			statement.executeUpdate("update autori set nume_autor = '"+autor.getNume()+"' where id = "+autor.getId());
		}else{
			statement.executeUpdate("insert into autori(nume_autor) values('"+autor.getNume()+"')");
		}
	}
	
	public static void afisare()throws SQLException{
		List<Autor> autori=findAll();
		for(Autor autor:autori){
			System.out.println(autor.getCarti().stream().map(Carte::getTitlu).collect(Collectors.toList()));
		}
	}
	
	public static void afisareDupaId(Integer id)throws SQLException{
		Autor autor=findById(id);
		System.out.println(autor.getCarti());
	}
	
	public static void main(String[] args) throws SQLException{
		
		afisareDupaId(1);
		afisare();
		/*System.out.println("Apasa 1 daca vrei sa adaugi un autor sau 2 daca vrei sa modifici un autor");
		Scanner scanner=new Scanner(System.in);
		int optiune=scanner.nextInt();
		if (optiune==1){
			Autori autor=new Autori();
			System.out.println("Introduceti nume autor: ");
			String nume=scanner.next();
			autor.setNume(nume);
			saveorupdateAutori(autor);
			afisare();
		}else{
			System.out.println("Introduceti id: ");
			int id=scanner.nextInt();
			Autori autor=findById(id);
			System.out.println("Introduceti nume autor: ");
			String nume=scanner.next();
			autor.setNume(nume);
			saveorupdateAutori(autor);
			afisare();
		}*/
	}
}


