package app1;

import java.util.List;

public class Autor {
	
	private Integer id;
	private String nume;
	private List<Carte> carti;
	
	public Autor(){
		
	}
	
	public Autor(Integer id,String nume){
		this.id=id;
		this.nume=nume;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public List<Carte> getCarti() {
		return carti;
	}

	public void setCarti(List<Carte> carti) {
		this.carti = carti;
	}

	@Override
	public String toString() {
		return "id=" + id + ", nume=" + nume + ", carti=" + carti;
	}

}	
