package app1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import app.IConstante;

public class DBCarti {
	
	public static List<Carte> findAll() throws SQLException{
		ResultSet result=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD).createStatement().executeQuery("select * from carti");
		System.out.println("Conexiunea noua tocmai fu creata");
		List<Carte> carti=new ArrayList<>();
		while(result.next()){
			Carte carte=new Carte();
			carte.setId(result.getInt("id"));
			carte.setTitlu(result.getString("titlu"));
			carti.add(carte);
		}
		return carti;
	}
	
	public static Carte findById(Integer id) throws SQLException{
		ResultSet result=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD).createStatement().executeQuery("select * from carti where id = "+id);
		System.out.println("Conexiunea noua tocmai fu creata");
		while(result.next()){
			Carte carte=new Carte();
			carte.setId(result.getInt("id"));
			carte.setTitlu(result.getString("titlu"));
			return carte;
		}
		return null;
	}
	
	public static List<Carte> findByAutorId(Integer id) throws SQLException{
		ResultSet result=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD).createStatement().executeQuery("select * from carti where id_autor = "+id);
		System.out.println("Conexiunea noua tocmai fu creata");
		List<Carte> carti=new ArrayList<>();
		while(result.next()){
			Carte carte=new Carte();
//			Autor autor=new Autor();
			carte.setId(result.getInt("id"));
			carte.setTitlu(result.getString("titlu"));
			//carte.setAutor(autor.setId(result.getInt("id_autor")),);
			carti.add(carte);
		}
		return carti;
	}
	
	public static void saveorupdateCarti(Carte carte) throws SQLException{
		Connection connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/"+
						 IConstante.NUMEDB,"root", IConstante.PASSWORD);
		Statement statement=connection.createStatement();
		if(carte.getId()!=null){
			statement.executeUpdate("update carti set titlu = '"+carte.getTitlu()+"' where id = "+carte.getId());
		}else{
			statement.executeUpdate("insert into carti(titlu) values('"+carte.getTitlu()+"')");
		}
	}
	
	public static void afisare()throws SQLException{
		List<Carte> carti=findAll();
		for(Carte carte:carti){
			System.out.println(carte.toString());
		}
	}
	
	public static void afisareDupaId(Integer id)throws SQLException{
		Carte carte=findById(id);
		System.out.println(carte);
	}
	
	public static void main(String[] args) throws SQLException{
		
		afisareDupaId(1);
		afisare();
		/*System.out.println("Apasa 1 daca vrei sa adaugi o carte sau 2 daca vrei sa modifici o carte");
		Scanner scanner=new Scanner(System.in);
		int optiune=scanner.nextInt();
		if (optiune==1){
			Carti carte=new Carti();
			System.out.println("Introduceti titlu carte: ");
			String titlu=scanner.next();
			carte.setTitlu(titlu);
			saveorupdateCarti(carte);
			afisare();
		}else{
			System.out.println("Introduceti id: ");
			int id=scanner.nextInt();
			Carti carte=findById(id);
			System.out.println("Introduceti titlu carte: ");
			String titlu=scanner.next();
			carte.setTitlu(titlu);
			saveorupdateCarti(carte);
			afisare();
		}*/
	}
	
}
