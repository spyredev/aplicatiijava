package app1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Program2 {

	public static void main(String[] args) throws SQLException {
		
		List<Carte> cartiAutor = null;
		
		System.out.println("Autor Name: ");
		Scanner scan = new Scanner(System.in);
		
		String numeAutor = scan.next();
		// cartiAutor = DBCarti.findByAutorId(numeAutor);
		cartiAutor = new ArrayList<>();
		// cartiAutor.addAll(DBAutori.findAll().stream().filter((p)->p.getNume().equals(numeAutor)).collect(Collectors.toList()).stream().map(Autor::getCarti).collect(Collectors.toList()));
		Autor autor = DBAutori.findByName(numeAutor);
//		cartiAutor = DBCarti.findByAutorId(autor.getId());
		
		System.out.println("Cartile sunt: " + autor.getCarti());
		
	}
	
}
